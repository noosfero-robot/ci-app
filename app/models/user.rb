class User < ActiveRecord::Base

  has_secure_password
  def self.digest(token)
    Digest::SHA1.hexdigest(token.to_s)
  end

end
