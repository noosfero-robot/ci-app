class MergeRequest < ActiveRecord::Base
  attr_accessible :project_id, :last_commit_sha, :url, :status

  def self.is_merge?(param)
    param.downcase == "merge_request"
  end


  def self.status(param)
    param.downcase
  end

end
