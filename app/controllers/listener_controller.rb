class ListenerController < ApplicationController

  require 'net/http'
  require 'uri'

  def  trigger
    mr = MergeRequest.find_by_mr_id(params[:object_attributes][:id])

    unless mr.nil?
      return if mr.last_commit_id == params[:object_attributes][:last_commit][:id]
      mr.last_commit_id = params[:object_attributes][:last_commit][:id]
      mr.commented = false
      mr.save
      system "curl --header 'PRIVATE-TOKEN: #{User.last.password}' --data-urlencode  'body=Running Build :)' 'https://gitlab.com/api/v3/projects/#{mr.target_project_id}/merge_requests/#{mr.mr_id}/notes'"
      updater(mr, params[:object_attributes][:target][:http_url])
    else
      mr  = MergeRequest.new
      mr.mr_id = params[:object_attributes][:id]
      mr.mr_url = params[:object_attributes][:url]
      mr.target_project_id  = params[:object_attributes][:target_project_id]
      mr.state = MergeRequest.status params[:object_attributes][:state]
      mr.last_commit_id = params[:object_attributes][:last_commit][:id]
      mr.src_branch = params[:object_attributes][:source_branch]
      mr.src_repo_url = params[:object_attributes][:source][:http_url]
      mr.commented = false
      project_id =  params[:object_attributes][:source_project_id]

      mr.save
      system "curl --header 'PRIVATE-TOKEN: #{User.last.password}' --data-urlencode  'body=Running Build :)' 'https://gitlab.com/api/v3/projects/#{mr.target_project_id}/merge_requests/#{mr.mr_id}/notes'"
      updater(mr, params[:object_attributes][:target][:http_url])
    end
  end

  def updater(mr, url)
    ci_remote = "git@gitlab.com:TallysMartins/noosfero.git"

    system "cd /home/tallys/tmp/builds/ && git clone #{url} project-#{mr.target_project_id}"

    system "cd /home/tallys/tmp/builds/project-#{mr.target_project_id} && git branch -D #{mr.src_branch}-#{mr.mr_id} "

    system "cd /home/tallys/tmp/builds/project-#{mr.target_project_id} && git remote set-url origin #{ci_remote} && git  push origin :#{mr.src_branch}-#{mr.mr_id} "

    system "cd /home/tallys/tmp/builds/project-#{mr.target_project_id} " +
    "&& git remote set-url origin #{mr.src_repo_url} && git fetch origin && git reset --hard origin/#{mr.src_branch} " +
     "&& git checkout -b #{mr.src_branch}-#{mr.mr_id} "+
     "&& git remote set-url origin #{ci_remote} && git push origin #{mr.src_branch}-#{mr.mr_id} "+
     "&& git checkout master"
  end

  def commit_build_url (params)
    "https://ci.gitlab.com/projects/" + "#{params[:project_id]}"  + "/" + "refs" + "/" + "#{params[:push_data][:ref]}" +"/" + "commits/" + "#{params[:sha]}"
  end

  def  trigger2
    url = commit_build_url(params)
    id = params[:ref].split("-").last
    mr = MergeRequest.find_by_mr_id(id)
    comment = "[Click Here For Build Status](#{url})"

    unless mr.commented
      system "curl --header 'PRIVATE-TOKEN: #{User.last.password}' --data-urlencode  'body=#{comment}' 'https://gitlab.com/api/v3/projects/#{mr.target_project_id}/merge_requests/#{mr.mr_id}/notes'"
      mr.commented = true
      mr.save
    end
  end

end
