class CreateMergeRequests < ActiveRecord::Migration
  def change
    create_table :merge_requests do |t|
      t.string :id
      t.string :mr_id
      t.string :mr_url
      t.string :target_project_id
      t.string :last_commit_id
      t.string :state
      t.string :build_status
      t.string :src_branch
      t.string :src_repo_url
      t.boolean :commented, :default => false
      t.timestamps
    end
  end
end
